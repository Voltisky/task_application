<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/29/18
 * Time: 12:07 AM
 */
namespace TaskBundle\Service;

use Doctrine\ORM\EntityManager;
use TaskBundle\Entity\LogEntry;

class LogAction
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addLog($objectType, $objectId, $logType){
        $log = new LogEntry();
        $log->setAction($logType);
        $log->setObjectId($objectId);
        $log->setObjectType($objectType);

        $this->em->persist($log);
        $this->em->flush();
    }
}