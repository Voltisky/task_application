<?php
namespace TaskBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use TaskBundle\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="TaskBundle\Entity\Task", mappedBy="assignedTo")
     */
    private $task;

    /**
     * Add task.
     *
     * @param \TaskBundle\Entity\Task $task
     *
     * @return User
     */
    public function addTask(\TaskBundle\Entity\Task $task)
    {
        $task->setAssignedTo($this);
        $this->task[] = $task;

        return $this;
    }

    /**
     * Remove task.
     *
     * @param \TaskBundle\Entity\Task $task
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTask(\TaskBundle\Entity\Task $task)
    {
        $task->setAssignedTo(null);
        return $this->task->removeElement($task);
    }

    /**
     * Get task.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTask()
    {
        return $this->task;
    }
}
