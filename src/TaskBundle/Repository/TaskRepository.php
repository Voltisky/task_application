<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/28/18
 * Time: 1:26 AM
 */

namespace TaskBundle\Repository;


use Doctrine\ORM\EntityRepository;
use TaskBundle\Entity\User;

class TaskRepository extends EntityRepository
{
    /**
     * Returns collection of tasks assigned to User
     * @param User $user
     * @return Task[]
     * @author Karol Włodek
     */
    public function findByUser(User $user)
    {
        $query = $this->createQueryBuilder('task')
            ->addSelect('assignedTo')
            ->innerJoin('task.assignedTo', 'assignedTo')
            ->where('assignedTo.id = :user_id')
            ->setParameter('user_id', $user->getId());

        return $query->getQuery()->getResult();
    }
}