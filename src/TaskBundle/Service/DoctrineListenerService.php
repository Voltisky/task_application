<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/29/18
 * Time: 12:41 AM
 */

namespace TaskBundle\Service;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TaskBundle\Entity\LogEntry;
use TaskBundle\Entity\Task;

class DoctrineListenerService implements EventSubscriber
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postUpdate',
        );
    }

    /**
     * This method will called on Doctrine postUpdate event
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $em = $args->getObjectManager();

        /** @var Task $object */
        $object = $args->getObject();

        /** @var UnitOfWork $uow */
        $uow = $em->getUnitOfWork();
        if($object instanceof Task)
        {
            $logAction = $this->container->get("app.log.action");
            $changeSet = $uow->getEntityChangeSet($args->getObject());
            foreach($changeSet as $columnName => $values)
            {
                switch($columnName)
                {
                    case "assignedTo":
                        $logAction->addLog(Task::class, $object->getId(), LogEntry::LOG_ASSIGN_USER);
                        break;
                    case "deadline":
                        $logAction->addLog(Task::class, $object->getId(), LogEntry::LOG_DEADLINE_SET);
                        break;
                    case "status":
                        $logAction->addLog(Task::class, $object->getId(), LogEntry::LOG_STATUS_CHANGE);
                        break;
                }
            }
        }
    }


}