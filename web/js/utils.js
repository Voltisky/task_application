var Utils = {};
Utils.IsDebug = (typeof IS_DEBUG !== "undefined") ? IS_DEBUG : false;
Utils.router = {
    "task_change_status": {
        url: "/api/task/{0}/change_status"
    },
    "task_show_panel": {
        url: "/api/task/{0}/show_panel"
    }
};

Utils.getUrl = function (name, params) {
    if (!Utils.router[name]) {
        return "";
    }

    var result = Utils.format(Utils.router[name].url, params);
    if (Utils.IsDebug === true) {
        result = "/app_dev.php" + result;
    }

    return result;
};

Utils.format = function (str, args) {
    var s = str;
    for (var i = 0; i < args.length; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
};
