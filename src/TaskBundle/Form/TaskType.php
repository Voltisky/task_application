<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/28/18
 * Time: 12:31 AM
 */

namespace TaskBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TaskBundle\Entity\Task;

class TaskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @author Karol Włodek
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ["label" => "app.task.field.title"])
            ->add('description', null, ["label" => "app.task.field.description"])
            ->add('assignedTo', null, ["label" => "app.task.field.assignedTo"])
            ->add('deadline', DateTimeType::class,
                ["required" => false, "date_widget" => "single_text", "label" => "app.task.field.deadline"])
            ->add('submit', SubmitType::class, ["attr" => ["class" => "btn-primary"], "label" => "app.submit"]);
    }

    /**
     * @param OptionsResolver $resolver
     * @author Karol Włodek
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Task::class
        ]);
    }
}