<?php

namespace TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use TaskBundle\Entity\LogEntry;

class DefaultController extends Controller
{
    /**
     * @Route("/log/entries", name="log_entries")
     * @author Karol Włodek
     */
    public function logAction(){
        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository(LogEntry::class)->findAll();

        return $this->render("TaskBundle:Default:log.html.twig", ["logs" => $logs]);
    }

    /**
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Karol Włodek
     */
    public function indexAction(){
        return $this->render("TaskBundle:Default:index.html.twig");
    }
}
