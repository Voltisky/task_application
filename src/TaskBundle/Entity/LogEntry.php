<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/28/18
 * Time: 7:00 PM
 */

namespace TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity
 * @ORM\Table(name="log_entry")
 */
class LogEntry
{
    const LOG_TASK_REMOVE = "app.log.task_remove";
    const LOG_ASSIGN_USER = "app.log.assign_user";
    const LOG_DEADLINE_SET = "app.log.deadline_set";
    const LOG_STATUS_CHANGE = "app.log.status_change";

    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $action;

    /**
     * Extra information to find Object
     * @ORM\Column(type="guid", nullable=true)
     */
    private $objectId;

    /**
     * Extra information to find Object
     * @ORM\Column(type="string", nullable=true)
     */
    private $objectType;

    /**
     * @var User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="TaskBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return LogEntry
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set objectId.
     *
     * @param string|null $objectId
     *
     * @return LogEntry
     */
    public function setObjectId($objectId = null)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId.
     *
     * @return string|null
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set objectType.
     *
     * @param string|null $objectType
     *
     * @return LogEntry
     */
    public function setObjectType($objectType = null)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType.
     *
     * @return string|null
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return LogEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy.
     *
     * @param \TaskBundle\Entity\User|null $createdBy
     *
     * @return LogEntry
     */
    public function setCreatedBy(\TaskBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \TaskBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
