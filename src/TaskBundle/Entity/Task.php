<?php
namespace TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TaskBundle\Entity\Traits\BlameableTrait;
use TaskBundle\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "task_show",
 *          parameters = { "task" = "expr(object.getId())" }
 *      )
 * )
 *
 * @ORM\Entity(repositoryClass="TaskBundle\Repository\TaskRepository")
 * @ORM\Table(name="task")
 */
class Task
{
    const STATUS_OCZEKUJACY = 10;
    const STATUS_WYKONANY = 20;
    const STATUS_ODRZUCONY = 30;

    const STATUSES_TRANS = [
        self::STATUS_OCZEKUJACY => "app.task.status.oczekujacy",
        self::STATUS_WYKONANY => "app.task.status.wykonany",
        self::STATUS_ODRZUCONY => "app.task.status.odrzucony"
    ];

    use TimestampableTrait;
    use BlameableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @ORM\OneToMany(targetEntity="TaskBundle\Entity\Task", mappedBy="parentTask")
     */
    private $childTasks;


    /**
     * @ORM\ManyToOne(targetEntity="TaskBundle\Entity\Task", inversedBy="childTasks")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parentTask;

    /**
     * @ORM\ManyToOne(targetEntity="TaskBundle\Entity\User", inversedBy="task")
     * @ORM\JoinColumn(nullable=true)
     */
    private $assignedTo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status = self::STATUS_OCZEKUJACY;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param mixed $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childTasks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add childTask.
     *
     * @param \TaskBundle\Entity\Task $childTask
     *
     * @return Task
     */
    public function addChildTask(\TaskBundle\Entity\Task $childTask)
    {
        $childTask->setParentTask($this);
        $this->childTasks[] = $childTask;

        return $this;
    }

    /**
     * Remove childTask.
     *
     * @param \TaskBundle\Entity\Task $childTask
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChildTask(\TaskBundle\Entity\Task $childTask)
    {
        $childTask->setParentTask(null);
        return $this->childTasks->removeElement($childTask);
    }

    /**
     * Get childTasks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildTasks()
    {
        return $this->childTasks;
    }

    /**
     * Set parentTask.
     *
     * @param \TaskBundle\Entity\Task|null $parentTask
     *
     * @return Task
     */
    public function setParentTask(\TaskBundle\Entity\Task $parentTask = null)
    {
        $this->parentTask = $parentTask;

        return $this;
    }

    /**
     * Get parentTask.
     *
     * @return \TaskBundle\Entity\Task|null
     */
    public function getParentTask()
    {
        return $this->parentTask;
    }

    /**
     * Set assignedTo.
     *
     * @param \TaskBundle\Entity\User|null $assignedTo
     *
     * @return Task
     */
    public function setAssignedTo(\TaskBundle\Entity\User $assignedTo = null)
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    /**
     * Get assignedTo.
     *
     * @return \TaskBundle\Entity\User|null
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @return mixed|string
     * @author Karol Włodek
     */
    public function getStatusText(){
        if(isset(self::STATUSES_TRANS[$this->getStatus()]))
        {
            return self::STATUSES_TRANS[$this->getStatus()];
        }
        else
        {
            return 'app.task.status.undefined';
        }
    }
}
