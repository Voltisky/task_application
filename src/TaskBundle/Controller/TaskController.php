<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/26/18
 * Time: 1:12 AM
 */

namespace TaskBundle\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TaskBundle\Entity\LogEntry;
use TaskBundle\Entity\Task;
use TaskBundle\Form\TaskType;

/**
 * @Route("/api")
 * Class TaskController
 * @package TaskBundle\Controller
 */
class TaskController extends FOSRestController
{
    /**
     * @Route("/task/{task}/delete", name="task_delete")
     * @param Task $task
     * @return Response
     * @author Karol Włodek
     */
    public function deleteAction(Task $task)
    {
        $em = $this->getDoctrine()->getManager();
        $tr = $this->get("translator");
        try {
            $em->remove($task);
            $em->flush();
            $this->addFlash("error", $tr->trans("app.task.delete_success"));

            $this->get('app.log.action')->addLog(Task::class, $task->getId(), LogEntry::LOG_TASK_REMOVE);
        } catch (\Exception $e) {
            $this->addFlash("danger", $tr->trans("app.task.delete_fail"));
        }

        return $this->redirectToRoute("task_list");
    }

    /**
     * @Route("/task/{task}/change_status", name="task_change_status")
     * @author Karol Włodek
     * @param Request $request
     * @param Task $task
     * @return Response
     */
    public function taskChangeStatusAction(Request $request, Task $task)
    {
        $em = $this->getDoctrine()->getManager();
        $status = $request->get("status");

        $statuses = [Task::STATUS_OCZEKUJACY, Task::STATUS_WYKONANY, Task::STATUS_ODRZUCONY];
        if (in_array($status, $statuses)) {
            $task->setStatus($status);

            $em->persist($task);
            $em->flush();

            return new Response("", Response::HTTP_OK);
        }

        return new Response("", Response::HTTP_BAD_REQUEST);
    }

    /**
     * @SWG\Response(
     *   response = 200,
     *     description="Returned when successful."
     * )
     *
     * @Rest\Get("/tasks", name="task_list")
     * @Rest\Get("/")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTasksAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Task::class);

        $view = $this->view($repo->findAll())->setTemplate("TaskBundle:Task:list.html.twig")->setTemplateVar('tasks');

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *   response = 200,
     *     description="Returned when successful."
     * )
     *
     * @Rest\Get("/tasks_assigned", name="task_list_assigned")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMyListAction()
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Task::class);

        $tasks = $repo->findByUser($user);

        $view = $this->view($tasks)->setTemplate("TaskBundle:Task:my_list.html.twig")->setTemplateVar('tasks');

        return $this->handleView($view);
    }

    /**
     *
     * @SWG\Response(
     *   response = 201,
     *     description="Created"
     * )
     *
     * @Rest\Post("/tasks", name="task_new_post")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task->setStatus(Task::STATUS_OCZEKUJACY);
            $em->persist($task);
            $em->flush();

            $this->addFlash("success", $this->get('translator')->trans("app.task.created"));

            return $this->redirectToRoute("task_edit", ["task" => $task->getId()]);
        } else {
            $errors = $form->getErrors();
            foreach ($errors as $error) {
                $this->addFlash("error", $error->getMessage());
            }
        }

        $view = $this->view($form->createView())->setTemplate('TaskBundle:Task:new.html.twig')->setTemplateVar("form");
        return $this->handleView($view);
    }

    /**
     *
     * @SWG\Response(
     *   response = 200,
     *     description="Success"
     * )
     *
     * @Rest\Get("/task/new", requirements={"_format" = "html"}, name="task_new")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newFormAction(Request $request)
    {
        $form = $this->createForm(TaskType::class);
        $view = $this->view($form->createView())->setTemplate('TaskBundle:Task:new.html.twig')->setTemplateVar("form");

        return $this->handleView($view);
    }

    /**
     * Get a specific post.
     *
     * @SWG\Response(
     *   response="200",
     *   description="Success",
     *   @Model(type=Task::class)
     * )
     *
     * @Rest\Get("/tasks/{task}", name="task_show", requirements={"id" = "\d+"})
     *
     * @return View
     */
    public function showAction(Task $task)
    {
        $view = $this->view($task)->setTemplate('TaskBundle:Task:show.html.twig')->setTemplateVar("task");
        return $this->handleView($view);
    }

    /**
     *
     * @SWG\Response(
     *   response = 200,
     *     description="Success"
     * )
     *
     * @Rest\Get("/tasks/{task}/edit", requirements={"_format" = "html"}, name="task_edit")
     *
     * @param Request $request
     * @param Task $task
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editFormAction(Task $task)
    {
        $form = $this->createForm(TaskType::class, $task);
        $view = $this->view($form->createView())->setTemplate('TaskBundle:Task:edit.html.twig')->setTemplateVar("form");

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *   response = 201,
     *     description="Created"
     * )
     *
     * @Rest\Post("/tasks/{task}", name="task_edit_post")
     *
     * @param Request $request
     * @param Task $task
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Task $task)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($task);
            $em->flush();

            $this->addFlash("success", $this->get('translator')->trans("app.task.modified"));

            return $this->redirectToRoute("task_show", ["task" => $task->getId()]);
        } else {
            $errors = $form->getErrors();
            foreach ($errors as $error) {
                $this->addFlash("error", $error->getMessage());
            }
        }

        $view = $this->view($form)->setTemplate('TaskBundle:Task:edit.html.twig')->setTemplateVar("form");
        return $this->handleView($view);
    }
}